﻿// <auto-generated />
using System;
using HrManagementTool.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace HrManagementTool.DAL.Migrations
{
    [DbContext(typeof(HrManagementContext))]
    [Migration("20190517065319_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasDefaultSchema("hr")
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("HrManagementTool.Domain.Position", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int")
                        .HasAnnotation("MySql:ValueGeneratedOnAdd", true);

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasColumnType("varchar(256)");

                    b.Property<string>("Title")
                        .HasColumnName("title")
                        .HasColumnType("varchar(128)");

                    b.HasKey("Id");

                    b.ToTable("Positions");
                });

            modelBuilder.Entity("HrManagementTool.Domain.Reason", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int")
                        .HasAnnotation("MySql:ValueGeneratedOnAdd", true);

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasColumnType("varchar(256)");

                    b.Property<sbyte>("IsPaid")
                        .HasColumnName("is_paid")
                        .HasColumnType("tinyint");

                    b.Property<string>("Title")
                        .HasColumnName("title")
                        .HasColumnType("varchar(128)");

                    b.HasKey("Id");

                    b.ToTable("Reasons");
                });

            modelBuilder.Entity("HrManagementTool.Domain.Request", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int")
                        .HasAnnotation("MySql:ValueGeneratedOnAdd", true);

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasColumnType("varchar(256)");

                    b.Property<DateTime>("FromDate")
                        .HasColumnName("from_date")
                        .HasColumnType("datetime");

                    b.Property<int>("Hours")
                        .HasColumnName("hours")
                        .HasColumnType("int");

                    b.Property<int?>("ReasonId");

                    b.Property<DateTime?>("ToDate")
                        .HasColumnName("to_date")
                        .HasColumnType("datetime");

                    b.Property<int?>("user_id");

                    b.HasKey("Id");

                    b.HasIndex("ReasonId");

                    b.HasIndex("user_id");

                    b.ToTable("Requests");
                });

            modelBuilder.Entity("HrManagementTool.Domain.User", b =>
                {
                    b.Property<int?>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int")
                        .HasAnnotation("MySql:ValueGeneratedOnAdd", true);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnName("created_at")
                        .HasColumnType("datetime");

                    b.Property<string>("FirstName")
                        .HasColumnName("fist_name")
                        .HasColumnType("varchar(64)");

                    b.Property<sbyte>("IsActive")
                        .HasColumnName("is_active")
                        .HasColumnType("tinyint");

                    b.Property<string>("PasswordHash")
                        .HasColumnName("password_hash")
                        .HasColumnType("varchar(512)");

                    b.Property<string>("SecondName")
                        .HasColumnName("second_name")
                        .HasColumnType("varchar(64)");

                    b.Property<string>("Username")
                        .HasColumnName("username")
                        .HasColumnType("varchar(128)");

                    b.Property<int?>("position_id");

                    b.HasKey("Id");

                    b.HasIndex("position_id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("HrManagementTool.Domain.UserPhoto", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id")
                        .HasColumnType("int")
                        .HasAnnotation("MySql:ValueGeneratedOnAdd", true);

                    b.Property<string>("ImageName")
                        .HasColumnName("image_name")
                        .HasColumnType("varchar(512)");

                    b.Property<int?>("user_id");

                    b.HasKey("Id");

                    b.HasIndex("user_id");

                    b.ToTable("UserPhotos");
                });

            modelBuilder.Entity("HrManagementTool.Domain.Request", b =>
                {
                    b.HasOne("HrManagementTool.Domain.Reason", "Reason")
                        .WithMany()
                        .HasForeignKey("ReasonId");

                    b.HasOne("HrManagementTool.Domain.User", "User")
                        .WithMany("Requests")
                        .HasForeignKey("user_id");
                });

            modelBuilder.Entity("HrManagementTool.Domain.User", b =>
                {
                    b.HasOne("HrManagementTool.Domain.Position", "Position")
                        .WithMany("Users")
                        .HasForeignKey("position_id");
                });

            modelBuilder.Entity("HrManagementTool.Domain.UserPhoto", b =>
                {
                    b.HasOne("HrManagementTool.Domain.User", "User")
                        .WithMany("UserPhotos")
                        .HasForeignKey("user_id");
                });
#pragma warning restore 612, 618
        }
    }
}
