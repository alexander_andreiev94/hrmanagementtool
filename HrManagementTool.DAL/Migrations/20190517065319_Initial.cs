﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HrManagementTool.DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "hr");

            migrationBuilder.CreateTable(
                name: "Positions",
                schema: "hr",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    title = table.Column<string>(type: "varchar(128)", nullable: true),
                    description = table.Column<string>(type: "varchar(256)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Positions", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Reasons",
                schema: "hr",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    title = table.Column<string>(type: "varchar(128)", nullable: true),
                    description = table.Column<string>(type: "varchar(256)", nullable: true),
                    is_paid = table.Column<sbyte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reasons", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "hr",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    username = table.Column<string>(type: "varchar(128)", nullable: true),
                    password_hash = table.Column<string>(type: "varchar(512)", nullable: true),
                    fist_name = table.Column<string>(type: "varchar(64)", nullable: true),
                    second_name = table.Column<string>(type: "varchar(64)", nullable: true),
                    is_active = table.Column<sbyte>(type: "tinyint", nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: false),
                    position_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.id);
                    table.ForeignKey(
                        name: "FK_Users_Positions_position_id",
                        column: x => x.position_id,
                        principalSchema: "hr",
                        principalTable: "Positions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Requests",
                schema: "hr",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    description = table.Column<string>(type: "varchar(256)", nullable: true),
                    hours = table.Column<int>(type: "int", nullable: false),
                    from_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    to_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    user_id = table.Column<int>(nullable: true),
                    ReasonId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Requests", x => x.id);
                    table.ForeignKey(
                        name: "FK_Requests_Reasons_ReasonId",
                        column: x => x.ReasonId,
                        principalSchema: "hr",
                        principalTable: "Reasons",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Requests_Users_user_id",
                        column: x => x.user_id,
                        principalSchema: "hr",
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserPhotos",
                schema: "hr",
                columns: table => new
                {
                    id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    image_name = table.Column<string>(type: "varchar(512)", nullable: true),
                    user_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPhotos", x => x.id);
                    table.ForeignKey(
                        name: "FK_UserPhotos_Users_user_id",
                        column: x => x.user_id,
                        principalSchema: "hr",
                        principalTable: "Users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Requests_ReasonId",
                schema: "hr",
                table: "Requests",
                column: "ReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_user_id",
                schema: "hr",
                table: "Requests",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_UserPhotos_user_id",
                schema: "hr",
                table: "UserPhotos",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_Users_position_id",
                schema: "hr",
                table: "Users",
                column: "position_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Requests",
                schema: "hr");

            migrationBuilder.DropTable(
                name: "UserPhotos",
                schema: "hr");

            migrationBuilder.DropTable(
                name: "Reasons",
                schema: "hr");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "hr");

            migrationBuilder.DropTable(
                name: "Positions",
                schema: "hr");
        }
    }
}
