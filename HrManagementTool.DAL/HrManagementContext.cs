using System;
using System.IO;
using HrManagementTool.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace HrManagementTool.DAL
{
    internal class HrManagementContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserPhoto> UserPhotos { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<Reason> Reasons { get; set; }
        public DbSet<Position> Positions { get; set; }

        public HrManagementContext()
        {
            
        }
        
        public HrManagementContext(DbContextOptions<HrManagementContext> options) : base(options)
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("settings.json")
                .Build();
        
            optionsBuilder.UseMySql(configuration.GetConnectionString("HrDatabase"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(schema: "hr");
            
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int")
                    .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.PasswordHash)
                    .HasColumnName("password_hash")
                    .HasColumnType("varchar(512)");

                entity.Property(e => e.FirstName)
                    .HasColumnName("fist_name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.SecondName)
                    .HasColumnName("second_name")
                    .HasColumnType("varchar(64)");

                entity.Property(e => e.IsActive)
                    .HasColumnName("is_active")
                    .HasColumnType("tinyint");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.HasOne(e => e.Position)
                    .WithMany(p => p.Users)
                    .HasForeignKey("position_id");
            });

            modelBuilder.Entity<UserPhoto>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int")
                    .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ImageName)
                    .HasColumnName("image_name")
                    .HasColumnType("varchar(512)");

                entity.HasOne(e => e.User)
                    .WithMany(u => u.UserPhotos)
                    .HasForeignKey("user_id");
            });

            modelBuilder.Entity<Reason>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int")
                    .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.IsPaid)
                    .HasColumnName("is_paid")
                    .HasColumnType("tinyint");
            });

            modelBuilder.Entity<Request>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int")
                    .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(256)");

                entity.Property(e => e.Hours)
                    .HasColumnName("hours")
                    .HasColumnType("int");

                entity.Property(e => e.FromDate)
                    .HasColumnName("from_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ToDate)
                    .HasColumnName("to_date")
                    .HasColumnType("datetime")
                    .IsRequired(false);

                entity.HasOne(e => e.User)
                    .WithMany(e => e.Requests)
                    .HasForeignKey("user_id");
            });

            modelBuilder.Entity<Position>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int")
                    .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasColumnType("varchar(128)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(256)");
            });
        }
    }
}