using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HrManagementTool.Domain.Abstracts;
using HrManagementTool.Domain;

namespace HrManagementTool.DAL.Repositories
{
    public class UserPhotoRepository : IRepository<UserPhoto>
    {
        private readonly HrManagementContext _context;

        public UserPhotoRepository()
        {
            _context = new HrManagementContext();
        }
        
        public void Dispose()
        {
            _context.Dispose();    
        }

        public void Add(UserPhoto entity)
        {
            _context.UserPhotos.Add(entity);
        }

        public void Update(UserPhoto entity)
        {
            _context.UserPhotos.Update(entity);
        }

        public void Delete(UserPhoto entity)
        {
            var photo = _context.UserPhotos.Find(entity.Id);

            if (photo != null)
                _context.UserPhotos.Remove(entity);
        }

        public IEnumerable<UserPhoto> GetMany()
        {
            return _context.UserPhotos.ToList();
        }

        public List<UserPhoto> GetMany(Expression<Func<UserPhoto, bool>> filter)
        {
            IQueryable<UserPhoto> userPhotos = _context.UserPhotos.Where(filter);
            return userPhotos.ToList();
        }

        public List<UserPhoto> GetMany(Expression<Func<UserPhoto, bool>> filter, int limit, int offset)
        {
            IQueryable<UserPhoto> userPhotos = _context.UserPhotos.Where(filter).Skip(offset).Take(limit);
            return userPhotos.ToList();
        }

        public UserPhoto GetOne(Expression<Func<UserPhoto, bool>> filter)
        {
            IQueryable<UserPhoto> userPhotos = _context.UserPhotos.Where(filter);
            return userPhotos.First();
        }

        public UserPhoto GetOne(int id)
        {
            return _context.UserPhotos.Find(id);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}