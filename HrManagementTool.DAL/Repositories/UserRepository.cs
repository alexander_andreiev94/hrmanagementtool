using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HrManagementTool.Domain.Abstracts;
using HrManagementTool.Domain;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace HrManagementTool.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly HrManagementContext _context;
        
        public UserRepository()
        {
            _context = new HrManagementContext();    
        }

        public void Add(User entity)
        {
            _context.Positions.Attach(entity.Position);
            _context.Users.Add(entity);
        }

        public void Update(User entity)
        {
            _context.Users.Update(entity);
        }

        public void Delete(User entity)
        {
            _context.Users.Remove(entity);
        }

        public IEnumerable<User> GetMany()
        {
            return _context.Users;
        }

        public List<User> GetMany(Expression<Func<User, bool>> filter)
        {
            IQueryable<User> result = _context.Users.Include(u => u.Position).Where(filter);
            return result.ToList();
        }

        public List<User> GetMany(Expression<Func<User, bool>> filter, int limit, int offset)
        {
            IQueryable<User> result = _context.Users.Include(u => u.Position).Where(filter).Take(limit);
            return result.ToList();
        }

        public User GetOne(Expression<Func<User, bool>> filter)
        {
            return _context.Users.Where(filter).First();
        }

        public User GetOne(int id)
        {
            return _context.Users.Include(u => u.Position).First(u => u.Id == id);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}