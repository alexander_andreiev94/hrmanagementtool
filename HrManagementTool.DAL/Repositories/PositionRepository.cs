using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HrManagementTool.Domain.Abstracts;
using HrManagementTool.Domain;

namespace HrManagementTool.DAL.Repositories
{
    public class PositionRepository : IRepository<Position>
    {
        private readonly HrManagementContext _context;
        
        public PositionRepository()
        {
            _context = new HrManagementContext();
        }
        
        public void Dispose()
        {
            _context.Dispose();
        }

        public void Add(Position entity)
        {
            _context.Positions.Add(entity);
        }

        public void Update(Position entity)
        {
            _context.Positions.Update(entity);
        }

        public void Delete(Position entity)
        {
            _context.Positions.Remove(entity);
        }

        public IEnumerable<Position> GetMany()
        {
            return _context.Positions.ToList();
        }

        public List<Position> GetMany(Expression<Func<Position, bool>> filter)
        {
            IQueryable<Position> positions = _context.Positions.Where(filter);
            return positions.ToList();
        }

        public List<Position> GetMany(Expression<Func<Position, bool>> filter, int limit, int offset)
        {
            IQueryable<Position> positions = _context.Positions.Where(filter).Skip(offset).Take(limit);
            return positions.ToList();
        }

        public Position GetOne(Expression<Func<Position, bool>> filter)
        {
            IQueryable<Position> positions = _context.Positions.Where(filter);
            return positions.First();
        }

        public Position GetOne(int id)
        {
            return _context.Positions.Find(id);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}