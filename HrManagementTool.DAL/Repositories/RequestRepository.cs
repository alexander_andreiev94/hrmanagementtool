using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HrManagementTool.Domain.Abstracts;
using HrManagementTool.Domain;

namespace HrManagementTool.DAL.Repositories
{
    public class RequestRepository : IRepository<Request>
    {
        private readonly HrManagementContext _context;
        
        public RequestRepository()
        {
            _context = new HrManagementContext();
        }
        
        public void Dispose()
        {
            _context.Dispose();
        }

        public void Add(Request entity)
        {
            _context.Requests.Add(entity);
        }

        public void Update(Request entity)
        {
            _context.Requests.Update(entity);
        }

        public void Delete(Request entity)
        {
            var request = _context.Requests.Find(entity.Id);

            if (request != null)
                _context.Requests.Remove(entity);
        }

        public IEnumerable<Request> GetMany()
        {
            return _context.Requests.ToList();
        }

        public List<Request> GetMany(Expression<Func<Request, bool>> filter)
        {
            IQueryable<Request> requests = _context.Requests.Where(filter);
            return requests.ToList();
        }

        public List<Request> GetMany(Expression<Func<Request, bool>> filter, int limit, int offset)
        {
            IQueryable<Request> requests = _context.Requests.Where(filter).Skip(offset).Take(limit);
            return requests.ToList();
        }

        public Request GetOne(Expression<Func<Request, bool>> filter)
        {
            IQueryable<Request> requests = _context.Requests.Where(filter);
            return requests.First();
        }

        public Request GetOne(int id)
        {
            return _context.Requests.Find(id);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}