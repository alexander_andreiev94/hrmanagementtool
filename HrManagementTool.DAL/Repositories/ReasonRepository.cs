using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HrManagementTool.Domain.Abstracts;
using HrManagementTool.Domain;

namespace HrManagementTool.DAL.Repositories
{
    public class ReasonRepository : IRepository<Reason>
    {
        private readonly HrManagementContext _context;
        
        public ReasonRepository()
        {
            _context = new HrManagementContext();    
        }
        
        public void Dispose()
        {
            _context.Dispose();
        }

        public void Add(Reason entity)
        {
            _context.Reasons.Add(entity);
        }

        public void Update(Reason entity)
        {
            _context.Reasons.Update(entity);
        }

        public void Delete(Reason entity)
        {
            var reason = _context.Reasons.Find(entity.Id);

            if (reason != null)
                _context.Reasons.Remove(entity);
        }

        public IEnumerable<Reason> GetMany()
        {
            return _context.Reasons.ToList();
        }

        public List<Reason> GetMany(Expression<Func<Reason, bool>> filter)
        {
            IQueryable<Reason> reasons = _context.Reasons.Where(filter);
            return reasons.ToList();
        }

        public List<Reason> GetMany(Expression<Func<Reason, bool>> filter, int limit, int offset)
        {
            IQueryable<Reason> reasons = _context.Reasons.Where(filter).Skip(offset).Take(limit);
            return reasons.ToList();
        }

        public Reason GetOne(Expression<Func<Reason, bool>> filter)
        {
            IQueryable<Reason> reasons = _context.Reasons.Where(filter);
            return reasons.First();
        }

        public Reason GetOne(int id)
        {
            return _context.Reasons.Find(id);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}