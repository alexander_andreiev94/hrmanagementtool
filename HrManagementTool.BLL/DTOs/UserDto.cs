using System;
using System.Collections.Generic;

namespace HrManagementTool.BLL.DTOs
{
    public class UserDto
    {
        public int? Id { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreateAt { get; set; }
        public PositionDto Position { get; set; }
        public List<string> UserPhotos { get; set; }
    }
}