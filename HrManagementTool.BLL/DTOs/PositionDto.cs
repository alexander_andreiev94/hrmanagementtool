namespace HrManagementTool.BLL.DTOs
{
    public class PositionDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}