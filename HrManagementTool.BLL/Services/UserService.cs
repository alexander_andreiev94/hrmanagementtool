using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using HrManagementTool.BLL.DTOs;
using HrManagementTool.DAL.Repositories;
using HrManagementTool.Domain;
using Newtonsoft.Json;

namespace HrManagementTool.BLL.Services
{
    public class UserService
    {
        public void Create(UserDto userDto)
        {
            User user = new User();
            
            user.Id = null;
            user.Username = userDto.Username;
            user.FirstName = userDto.FirstName;
            user.SecondName = userDto.SecondName;
            user.IsActive = userDto.IsActive;
            user.PasswordHash = userDto.PasswordHash;
            user.CreatedAt = userDto.CreateAt;

            using (var repository = new PositionRepository())
            {
                Position position = repository.GetOne(p => p.Id == userDto.Position.Id);
                if (position != null)
                    user.Position = position;
            }
            
            using (var repository = new UserRepository())
            {
                repository.Add(user);
                repository.Save();
            }
            
            if (userDto.UserPhotos != null)
            {
                using (var repository = new UserPhotoRepository())
                {
                    foreach (var photo in userDto.UserPhotos)
                    {
                        repository.Add(new UserPhoto() { ImageName = photo, User = user });
                    }
                    
                    repository.Save();
                }
            }
        }

        public List<UserDto> GetUsers(int offset, int limit)
        {
            using (var repository = new UserRepository())
            {
                //var users = repository.GetMany(u => u.Id > offset, limit, offset) as List<User>;
                var users = repository.GetMany(u => u.Id >= offset, limit, offset);

                List<UserDto> userDtos = new List<UserDto>();
                
                foreach (User user in users)
                {                
                    userDtos.Add(new UserDto()
                    {
                        Id = user.Id,
                        Username = user.Username,
                        FirstName = user.FirstName,
                        SecondName = user.SecondName,
                        CreateAt = user.CreatedAt,
                        IsActive = user.IsActive,
                        Position = new PositionDto() { Id = user.Position.Id, Title = user.Position.Title },
                        PasswordHash = user.PasswordHash,
                        UserPhotos = new List<string>()
                    });    
                }
                
                return userDtos;
            }
        }

        public UserDto GetOne(int id)
        {
            using (var repository = new UserRepository())
            {
                User user = repository.GetOne(id);
                
                List<string> userPhotosDto = new List<string>();

                using (var photoRepo = new UserPhotoRepository())
                {
                    var photos = photoRepo.GetMany(up => up.User == user);
                    if (photos != null)
                    {
                        foreach (UserPhoto userPhoto in photos)
                        {
                            userPhotosDto.Add(userPhoto.ImageName);
                        }
                    }
                }
                
                UserDto userDto = new UserDto()
                {
                    Id = user.Id,
                    Username = user.Username,
                    FirstName = user.FirstName,
                    SecondName = user.SecondName,
                    CreateAt = user.CreatedAt,
                    IsActive = user.IsActive,
                    Position = new PositionDto() { Id = user.Position.Id, Title = user.Position.Title },
                    PasswordHash = user.PasswordHash,
                    UserPhotos = userPhotosDto
                };

                return userDto;
            }
        }

        public int GetUserCount()
        {
            using (var repository = new UserRepository())
            {
                return repository.GetMany().Count();
            }
        }

        public string EncryptPassword(string password)
        {
            byte[] encBuffer = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(password));
            return Convert.ToBase64String(encBuffer);
        }

        public void Remove(int id)
        {
            using (var repository = new UserRepository())
            {
                User user = repository.GetOne(id);
                if (user != null)
                {
                    repository.Delete(user);
                    repository.Save();
                }     
            }
        }
    }
}