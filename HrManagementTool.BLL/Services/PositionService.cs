using System.Collections.Generic;
using HrManagementTool.BLL.DTOs;
using HrManagementTool.DAL.Repositories;
using HrManagementTool.Domain;

namespace HrManagementTool.BLL.Services
{
    public class PositionService
    {
        public List<PositionDto> GetPositionList()
        {
            using (var repository = new PositionRepository())
            {
                List<Position> positions = repository.GetMany() as List<Position>;
                
                List<PositionDto> positionDtos = new List<PositionDto>();

                foreach (Position position in positions)
                {
                    positionDtos.Add(new PositionDto()
                    {
                        Id = position.Id,
                        Title = position.Title
                    });
                }

                return positionDtos;
            }
        }

        public PositionDto GetOne(int id)
        {
            using (var repository = new PositionRepository())
            {
                Position position = repository.GetOne(id);

                return new PositionDto()
                {
                    Id = position.Id,
                    Title = position.Title
                };
            }
        }
    }
}