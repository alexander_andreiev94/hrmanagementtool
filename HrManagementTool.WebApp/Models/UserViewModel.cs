using System;
using System.ComponentModel.DataAnnotations;
using HrManagementTool.BLL.DTOs;

namespace HrManagementTool.WebApp.Models
{
    public class UserViewModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string SecondName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Please specify position")]
        public int PositionId { get; set; }
        public bool IsEdit { get; set; }
    }
}