$(document).ready(function() {
    $('#user-dataTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "/User/Load",
        deferRender: true,
        columns: [
            {
                title: "Id",
                name: "Id",
                data: "id"
            },
            {
                title: "Username",
                name: "Username",
                data: "username"
            },
            {
                title: "First name",
                name: "First name",
                data: "firstName"
            },
            {
                title: "Second name",
                name: "Second name",
                data: "secondName"
            },
            {
                title: "Is active",
                name: "Is active",
                data: "isActive",
                render: (data, type, row) => {
                    return row.isActive ? "Active" : "Not active";
                }
            },
            {
                title: "Created At",
                name: "Created At",
                data: "createAt",
                render: (data, type, row) => {
                    return moment(row.createAt).format("YYYY-MM-DD hh:mm:ss");
                }
            },
            {
                title: "Position",
                name: "Position",
                data: "position",
                render: (data, type, row) => {
                    return row.position.title;
                }
            },
            {
                data: null,
                render: (data, type, row) => {
                    
                    return "<td>" +
                           '<a href="/User/Edit/' + row.id +'" class="btn btn-warning">Edit</a>' +
                           "</td>" 
                }
            },
            {
                data: null,
                render: (data, type, row) => {

                    return "<td>" +
                        '<a href="/User/Remove/' + row.id + '" class="btn btn-danger">Remove</a>' +
                        "</td>"
                }
            }
        ]
    });
});