using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HrManagementTool.BLL.DTOs;
using HrManagementTool.BLL.Services;
using HrManagementTool.Domain;
using HrManagementTool.WebApp.Models;
using JqueryDataTables.ServerSide.AspNetCoreWeb;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HrManagementTool.WebApp.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly UserService _userService;
        private readonly PositionService _positionService;

        public UserController(UserService userService, PositionService positionService)
        {
            _userService = userService;
            _positionService = positionService;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Load([FromQuery] DTParameters model)
        {
            List<UserDto> users = _userService.GetUsers(model.Start + 1, model.Length);
  
            int c = _userService.GetUserCount();
            var result = new DTResult<UserDto>
            {
                draw = model.Draw,
                data = users.ToArray(),
                recordsTotal = c,
                recordsFiltered = c
            };
 
            return new JsonResult(result);
        }

        
        public IActionResult Create()
        {
            ViewBag.positions = _positionService.GetPositionList();
            
            return View(new UserViewModel());
        }

        [HttpPost]
        public IActionResult Create([FromForm] UserViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Some fields are missing or invalid");
                ViewBag.positions = _positionService.GetPositionList();
                
                return View(model);
            }
            
            _userService.Create(new UserDto()
            {
                Username = model.Username,
                CreateAt = model.CreatedAt,
                FirstName = model.FirstName,
                IsActive = model.IsActive,
                SecondName = model.SecondName,
                Position = _positionService.GetOne(model.PositionId),
                PasswordHash = _userService.EncryptPassword(model.Password)
            });

            return RedirectToAction(nameof(Index));
        }
        
        public IActionResult Edit(int id)
        {
            UserDto user = _userService.GetOne(id);
            
            ViewBag.positions = _positionService.GetPositionList();
            
            return View(new UserViewModel()
            {
                CreatedAt = user.CreateAt,
                FirstName = user.FirstName,
                IsActive = user.IsActive,
                PositionId = user.Position.Id,
                SecondName = user.SecondName,
                Username = user.Username,
                IsEdit = true
            });
        }

        [HttpPost]
        public IActionResult Edit([FromForm] UserViewModel model)
        {
            Console.WriteLine(JsonConvert.SerializeObject(model));
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Remove(int id)
        {
            _userService.Remove(id);
            return RedirectToAction(nameof(Index));
        }
    }
}