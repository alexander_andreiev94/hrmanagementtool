using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using HrManagementTool.BLL.DTOs;
using HrManagementTool.BLL.Services;
using System.Collections.Generic;
using JqueryDataTables.ServerSide.AspNetCoreWeb;

namespace HrManagementTool.WebApp.Controllers
{
    [Authorize]
    public class PositionController : Controller
    {
        private readonly PositionService _positionService;

        public PositionController(PositionService positionService)
        {
            _positionService = positionService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Load([FromQuery] DTParameters parameters)
        {
            List<PositionDto> positions = _positionService.GetPositionList(parameters.Start + 1, parameters.Length);
            
            int count = _positionService.GetPositionCount();

            return new JsonResult(new DTResult<PositionDto>() {
                draw = parameters.Draw,
                data = positions.ToArray(),
                recordsTotal = count,
                recordsFiltered = count
            });
        }
    }
}