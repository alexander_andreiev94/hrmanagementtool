using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HrManagementTool.Domain.Abstracts
{
    public interface IRepository<TEntity> : IDisposable
    {
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        IEnumerable<TEntity> GetMany();
        List<TEntity> GetMany(Expression<Func<TEntity, bool>> filter);
        List<TEntity> GetMany(Expression<Func<TEntity, bool>> filter, int limit, int offset);
        TEntity GetOne(Expression<Func<TEntity, bool>> filter);
        TEntity GetOne(int id);
        void Save();
        Task SaveAsync();
    }
}