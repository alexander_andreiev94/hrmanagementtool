using System.Collections.Generic;

namespace HrManagementTool.Domain
{
    public class Position
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public ICollection<User> Users { get; set; }
    }
}