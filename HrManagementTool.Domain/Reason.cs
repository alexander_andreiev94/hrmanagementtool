namespace HrManagementTool.Domain
{
    public class Reason
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsPaid { get; set; }
    }
}