namespace HrManagementTool.Domain
{
    public class UserPhoto
    {
        public int Id { get; set; }
        public string ImageName { get; set; }
        public User User { get; set; }
    }
}