using System;
using System.Collections.Generic;

namespace HrManagementTool.Domain
{
    public class User
    {
        public int? Id { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public Position Position { get; set; }
        public ICollection<UserPhoto> UserPhotos { get; set; }
        public ICollection<Request> Requests { get; set; }
    }
}