using System;

namespace HrManagementTool.Domain
{
    public class Request
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int Hours { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public User User { get; set; }
        public Reason Reason { get; set; }
    }
}